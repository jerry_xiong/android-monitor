package com.jerryx.tool.android.bean;

import com.jerryx.tool.android.command.CheckPackageCommand;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jexiong on 9/24/2015.
 */
public class PackageInfoBeanTest {

    private CheckPackageCommand.PackageInfoBean packageInfoBean = new CheckPackageCommand.PackageInfoBean();

    @Test
    public void testMemoryBind() {
        String line1 = "package:com.android.vpndialogs";
        String line2 = "package:com.sec.android.app.lcdtest";
        String line3 = "package:com.sec.android.service.cm";
        String line4 = "package:com.samsung.shareshot";
        String line5 = "package:com.jerry.dummy";
        packageInfoBean.bind(line1);
        packageInfoBean.bind(line2);
        packageInfoBean.bind(line3);
        packageInfoBean.bind(line4);
        packageInfoBean.bind(line5);
        Assert.assertTrue(packageInfoBean.containsPackage("com.jerry.dummy"));
        Assert.assertFalse(packageInfoBean.containsPackage("jerry.dummy"));
    }
}
