package com.jerryx.tool.android.bean;

import com.jerryx.tool.android.command.RunTestCommand;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jexiong on 10/30/2015.
 */
public class TestBeanTest {

    @Test
    public void testBeanFail() {
        RunTestCommand.TestBean testBean = new RunTestCommand.TestBean();
        testBean.bind("Time: 15.247");
        testBean.bind("FAILURES!!!");
        testBean.bind("Tests run: 1,  Failures: 1");
        testBean.verify();
        Assert.assertFalse(testBean.isPassed());
        Assert.assertEquals("15.2", testBean.cost);
        Assert.assertEquals(1, testBean.failedCount);
        Assert.assertEquals(1, testBean.totalCount);
        Assert.assertEquals(0, testBean.passCount);
    }

    @Test
    public void testBeansFail() {
        RunTestCommand.TestBean testBean = new RunTestCommand.TestBean();
        testBean.bind("Time: 25.247");
        testBean.bind("FAILURES!!!");
        testBean.bind("Tests run: 3,  Failures: 3");
        testBean.verify();
        Assert.assertFalse(testBean.isPassed());
        Assert.assertEquals("25.2", testBean.cost);
        Assert.assertEquals(3, testBean.failedCount);
        Assert.assertEquals(3, testBean.totalCount);
        Assert.assertEquals(0, testBean.passCount);
    }

    @Test
    public void testBeanSuccess() {
        RunTestCommand.TestBean testBean = new RunTestCommand.TestBean();
        testBean.bind("Time: 31.10");
        testBean.bind("Test com.jerry.test.TestCase1");
        testBean.bind("OK (1 test)");
        testBean.verify();
        Assert.assertTrue(testBean.isPassed());
        Assert.assertEquals("31.1", testBean.cost);
        Assert.assertEquals(0, testBean.failedCount);
        Assert.assertEquals(1, testBean.totalCount);
        Assert.assertEquals(1, testBean.passCount);
    }

    @Test
    public void testBeansSuccess() {
        RunTestCommand.TestBean testBean = new RunTestCommand.TestBean();
        testBean.bind("Time: 41.10");
        testBean.bind("Test com.jerry.test.TestCase1");
        testBean.bind("Test com.jerry.test.TestCase2");
        testBean.bind("Test com.jerry.test.TestCase3");
        testBean.bind("OK (3 tests)");
        testBean.verify();
        Assert.assertTrue(testBean.isPassed());
        Assert.assertEquals("41.1", testBean.cost);
        Assert.assertEquals(0, testBean.failedCount);
        Assert.assertEquals(3, testBean.totalCount);
        Assert.assertEquals(3, testBean.passCount);
    }
}
