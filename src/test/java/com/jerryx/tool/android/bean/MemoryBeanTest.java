package com.jerryx.tool.android.bean;

import com.jerryx.tool.android.command.DumpMemCommand;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jexiong on 7/15/2015.
 */
public class MemoryBeanTest {

    private DumpMemCommand.MemoryBean memoryBean = new DumpMemCommand.MemoryBean();

    @Test
    public void testMemoryBind() {
        String line1 = "Native Heap     6213     6192        0    32575     6692    44888    39806     5081";
        String line2 = "Dalvik Heap     6525     6356       64      451     7136    17924     5636    12288";
        String line3 = "TOTAL    76189    28052     4612    35479    76189    62812    45442    17369";
        memoryBean.bind(line1);
        memoryBean.bind(line2);
        memoryBean.bind(line3);
        Assert.assertEquals(6213, memoryBean.nativeHeap);
        Assert.assertEquals(6525, memoryBean.dalvikHeap);
        Assert.assertEquals(76189, memoryBean.total);
    }
}
