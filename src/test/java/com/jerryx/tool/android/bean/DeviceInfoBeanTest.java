package com.jerryx.tool.android.bean;

import com.jerryx.tool.android.command.DeviceInfoCommand;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by jexiong on 7/15/2015.
 */
public class DeviceInfoBeanTest {

    @Test
    public void deviceInfoBindTest() {
        DeviceInfoCommand.DeviceInfoBean bean = new DeviceInfoCommand.DeviceInfoBean();
        bean.bind("[ro.product.model]: [Nexus 5]");
        bean.bind("[ro.build.version.release]: [5.1.1]");
        bean.bind("[ro.build.version.sdk]: [22]");
        bean.bind("[dalvik.vm.heapgrowthlimit]: [192m]");
        bean.bind("[ro.product.cpu.abi2]: [armeabi]");
        bean.bind("[persist.sys.timezone]: [Asia/Shanghai]");
        bean.bind("[dhcp.wlan0.ipaddress]: [10.108.27.60]");

        Assert.assertEquals("Nexus 5", bean.getInfoMap().get("ro.product.model"));
        Assert.assertEquals("5.1.1", bean.getInfoMap().get("ro.build.version.release"));
        Assert.assertEquals("22", bean.getInfoMap().get("ro.build.version.sdk"));
        Assert.assertEquals("192m", bean.getInfoMap().get("dalvik.vm.heapgrowthlimit"));
        Assert.assertEquals("armeabi", bean.getInfoMap().get("ro.product.cpu.abi2"));
        Assert.assertEquals("Asia/Shanghai", bean.getInfoMap().get("persist.sys.timezone"));
        Assert.assertEquals("10.108.27.60", bean.getInfoMap().get("dhcp.wlan0.ipaddress"));
    }
}
