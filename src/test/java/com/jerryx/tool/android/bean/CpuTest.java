package com.jerryx.tool.android.bean;

import com.jerryx.tool.android.command.DumpCpuCommand;
import com.jerryx.tool.android.util.MonitorCenter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jerry on 7/16/15.
 */
public class CpuTest {

    private DumpCpuCommand.CpuBean cpuBean = new DumpCpuCommand.CpuBean();

    @Before
    public void setUp() {
        MonitorCenter.setAppPackageName("com.jerry.dummy");
    }

    @Test
    public void testCPUBind() {
        String line = "4.1% 5617/com.jerry.dummy: 3.8% user + 0.2% kernel / faults: 38 minor";
        cpuBean.bind(line);
        Assert.assertEquals(4.1f, cpuBean.total, 0);
        Assert.assertEquals(3.8f, cpuBean.user, 0);
        Assert.assertEquals(0.2f, cpuBean.kernel, 0);
    }
}
