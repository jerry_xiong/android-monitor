Applications Memory Usage (in Kilobytes):
Uptime: 22222996 Realtime: 113544024

** MEMINFO in pid 4286 [com.ctsi.emm] **
                   Pss  Private  Private  SwapPss      Rss     Heap     Heap     Heap
                 Total    Dirty    Clean    Dirty    Total     Size    Alloc     Free
                ------   ------   ------   ------   ------   ------   ------   ------
  Native Heap     6213     6192        0    32575     6692    44888    39806     5081
  Dalvik Heap     6525     6356       64      451     7136    17924     5636    12288
 Dalvik Other     4766     3788        0      165     6184
        Stack      608      608        0      868      616
       Ashmem        0        0        0        0       12
    Other dev      145        0      144        0      476
     .so mmap     2226      128       76      399    57724
    .jar mmap      869        0       68        0    36304
    .apk mmap      404        0       16        0    16444
    .ttf mmap     1776        0        0        0    12100
    .dex mmap     5121        0     4136       12     6256
    .oat mmap      269        0        0        0    16252
    .art mmap     3258     2604       56      520    13656
   Other mmap      187       36       52        4     3484
    GL mtrack     8204     8204        0        0     8204
      Unknown      139      136        0      485      264
        TOTAL    76189    28052     4612    35479    76189    62812    45442    17369

 App Summary
                       Pss(KB)                        Rss(KB)
                        ------                         ------
           Java Heap:     9016                          20792
         Native Heap:     6192                           6692
                Code:     5872                         148560
               Stack:      608                            616
            Graphics:     8204                           8204
       Private Other:     2772
              System:    43525
             Unknown:                                    6940

           TOTAL PSS:    76189            TOTAL RSS:   191804       TOTAL SWAP PSS:    35479

 Objects
               Views:       77         ViewRootImpl:        1
         AppContexts:        9           Activities:        1
              Assets:       10        AssetManagers:        0
       Local Binders:       59        Proxy Binders:       90
       Parcel memory:       17         Parcel count:       67
    Death Recipients:        4      OpenSSL Sockets:        2
            WebViews:        0

 SQL
         MEMORY_USED:     2249
  PAGECACHE_OVERFLOW:      746          MALLOC_SIZE:      640

 DATABASES
      pgsz     dbsz   Lookaside(b)          cache  Dbname
         4       20             74     128/1061/3  /data/user/0/com.ctsi.emm/databases/ncm.db
         4       12             19       0/1060/1  /data/user/0/com.ctsi.emm/databases/agent.db
         4      132             96   7358/5411/25  /data/user/0/com.ctsi.emm/no_backup/androidx.work.workdb
         4        8                         0/0/0    (attached) temp
         4      132             48     360/1061/5  /data/user/0/com.ctsi.emm/no_backup/androidx.work.workdb (2)
         4      392             95  11642/6286/25  /data/user/0/com.ctsi.emm/databases/mdm.db
         4       28                         0/0/0    (attached) temp
         4      392             29      70/1053/2  /data/user/0/com.ctsi.emm/databases/mdm.db (63)
         4      392             94    744/1120/25  /data/user/0/com.ctsi.emm/databases/mdm.db (61)
         4      392            109     225/1347/7  /data/user/0/com.ctsi.emm/databases/mdm.db (62)
