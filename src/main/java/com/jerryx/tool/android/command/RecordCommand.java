package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;

/**
 * Created by jexiong on 7/15/2015.
 */
public class RecordCommand extends Command {

    private String fileName;

    @Override
    public String[] getCommandInputArray() {
        setFileName();
        return new String[]{"adb shell screenrecord " + fileName};
    }

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }

    public String getFileName() {
        return fileName;
    }

    private void setFileName() {
        this.fileName = Utils.getArtifactPath() + Utils.getTimestampString() + ".mp4";
    }
}
