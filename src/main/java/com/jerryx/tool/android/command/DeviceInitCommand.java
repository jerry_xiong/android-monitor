package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;

/**
 * Created by jexiong on 7/15/2015.
 */
public class DeviceInitCommand extends Command {

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }

    @Override
    public String[] getCommandInputArray() {
        String[] array = {
                "adb shell mkdir " + Utils.getArtifactPath(),
                "adb shell mkdir " + Utils.getAutomationPath(),
                "adb push data/automation.properties " + Utils.getAutomationPath() + "automation.properties"
        };
        return array;
    }
}
