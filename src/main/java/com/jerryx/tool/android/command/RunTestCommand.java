package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;

public class RunTestCommand extends Command<RunTestCommand.TestBean> {

    private String testCase;
    private String testPackage;
    private String testRunner;

    TestBean resultBean = new TestBean();

    public RunTestCommand(String testCase) {
        this.testCase = testCase;
        this.testPackage = Utils.getTestConfigValue("test.package");
        this.testRunner = Utils.getTestConfigValue("test.runner");
    }

    @Override
    public TestBean getCommandOutput() {
        return resultBean;
    }

    @Override
    public String[] getCommandInputArray() {
        StringBuilder cmd = new StringBuilder();
        cmd.append("adb shell am instrument -w -e class ");
        cmd.append(testCase).append(" ");
        cmd.append(testPackage).append("/");
        cmd.append(testRunner);
        return new String[]{cmd.toString()};
    }

    public String getShortTestCaseName() {
        int index = testCase.lastIndexOf(".");
        if (index != -1) {
            return testCase.substring(index + 1);
        }
        return testCase;
    }

    public static class TestBean implements AbstractBean {

        private StringBuilder content = new StringBuilder();

        public int totalCount;
        public int passCount;
        public int failedCount;
        public String cost;

        public void bind(String line) {
            if (line != null && line.trim().length() > 0) {
                content.append(line);
                content.append(Utils.NEW_LINE);
            }
        }

        public StringBuilder getContent() {
            return content;
        }

        public void verify() {
            String arg0 = "Time: ";
            String arg1 = "Tests run: ";
            String arg2 = "Failures: ";
            String arg3 = "OK (";
            String arg4 = "test)";
            String arg5 = "tests)";

            int index0 = content.indexOf(arg0);
            int index1 = content.indexOf(arg1);
            int index2 = content.indexOf(arg2);
            int index3 = content.indexOf(arg3);
            int index4 = content.indexOf(arg4);
            int index5 = content.indexOf(arg5);

            if (index0 != -1) {
                cost = content.substring(index0 + arg0.length(), index0 + arg0.length() + 4);
            }
            if (index1 != -1) {
                totalCount = Integer.parseInt(content.substring(index1 + arg1.length(), index1 + arg1.length() + 1));
                if (index2 != -1) {
                    failedCount = Integer.parseInt(content.substring(index2 + arg2.length(), index2 + arg2.length() + 1));
                }
            }
            if (index3 != -1 && index4 != -1) {
                totalCount = Integer.parseInt(content.substring(index3 + arg3.length(), index3 + arg3.length() + 1));
                passCount = totalCount;
            }
            if (index3 != -1 && index5 != -1) {
                totalCount = Integer.parseInt(content.substring(index3 + arg3.length(), index3 + arg5.length() - 1));
                passCount = totalCount;
            }
        }

        public boolean isPassed() {
            return totalCount == passCount;
        }
    }

}
