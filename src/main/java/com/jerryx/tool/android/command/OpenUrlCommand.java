package com.jerryx.tool.android.command;

public class OpenUrlCommand extends Command<AbstractBean> {

    private String url;

    public OpenUrlCommand(String url) {
        this.url = url;
    }

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{
                // only works for windows now, will support mac soon.
                "rundll32 url.dll,FileProtocolHandler " + url
        };
    }

}
