package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jexiong on 7/15/2015.
 */
public class ReportCommand extends Command<ReportCommand.ReportBean> {

    private ReportBean reportBean = new ReportBean();

    @Override
    public ReportBean getCommandOutput() {
        return reportBean;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{
                "adb pull " + Utils.getArtifactPath() + " " + Utils.getLogPath(),
                "adb pull /data/anr/traces.txt " + Utils.getLogPath()
        };
    }

    public static class ReportBean implements AbstractBean {

        private int maxMemoryUsage;
        private Date maxMemoryTime;
        private float maxCpuUsage;
        private Date maxCpuTime;
        private List<String> fileList = new ArrayList<String>();

        public int getMaxMemoryUsage() {
            return maxMemoryUsage;
        }

        public void setMaxMemoryUsage(int maxMemoryUsage) {
            this.maxMemoryUsage = maxMemoryUsage;
        }

        public void setMaxMemoryTime(Date maxMemoryTime) {
            this.maxMemoryTime = maxMemoryTime;
        }

        public float getMaxCpuUsage() {
            return maxCpuUsage;
        }

        public void setMaxCpuUsage(float maxCpuUsage) {
            this.maxCpuUsage = maxCpuUsage;
        }

        public void setMaxCpuTime(Date maxCpuTime) {
            this.maxCpuTime = maxCpuTime;
        }

        public List<String> getFileList() {
            return fileList;
        }

        @Override
        public String toString() {
            StringBuilder report = new StringBuilder();
            report.append("--------------------Monitor Report--------------------").append(Utils.NEW_LINE).append(Utils.NEW_LINE);
            report.append("Max Memory Usage: ").append(maxMemoryUsage).append("KB").append(Utils.NEW_LINE);
            report.append("Max Memory Time: ").append(maxMemoryTime).append("\r\n").append(Utils.NEW_LINE);
            report.append("Max CPU Usage: ").append(maxCpuUsage).append("%").append(Utils.NEW_LINE);
            report.append("Max CPU Time: ").append(maxCpuTime).append(Utils.NEW_LINE).append(Utils.NEW_LINE);
            report.append("CPU Tracking File: ").append(Utils.getLogPath()).append("cpu_log.txt").append(Utils.NEW_LINE);
            report.append("Memory Tracking File: ").append(Utils.getLogPath()).append("mem_log.txt").append(Utils.NEW_LINE).append(Utils.NEW_LINE);
            report.append("Captured Screenshots or video:\r\n");
            for (int i = 0; i < fileList.size(); i++) {
                report.append(fileList.get(i)).append("\r\n");
            }
            return report.toString();
        }

        public void bind(String line) {
            // do nothing
        }
    }

}
