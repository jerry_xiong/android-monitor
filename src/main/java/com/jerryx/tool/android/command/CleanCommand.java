package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;

public class CleanCommand extends Command {

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb shell rm -rf " + Utils.getArtifactPath()};
    }

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }
}
