package com.jerryx.tool.android.command;

import java.util.HashSet;
import java.util.Set;

public class CheckPackageCommand extends Command<CheckPackageCommand.PackageInfoBean> {

    PackageInfoBean resultBean = new PackageInfoBean();

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb shell pm list package"};
    }

    @Override
    public PackageInfoBean getCommandOutput() {
        return resultBean;
    }

    public static class PackageInfoBean implements AbstractBean {

        private Set<String> packageSet = new HashSet<String>();

        public void bind(String line) {
            int indexSeparator = line.indexOf(":");
            if (indexSeparator != -1) {
                String value = line.substring(indexSeparator + 1);
                packageSet.add(value);
            }
        }

        public boolean containsPackage(String packageName) {
            return packageSet != null && packageSet.contains(packageName);
        }
    }

}
