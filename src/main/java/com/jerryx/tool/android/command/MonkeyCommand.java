package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.MonitorCenter;

/**
 * Created by jexiong on 7/15/2015.
 */
public class MonkeyCommand extends Command<AbstractBean> {

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb shell monkey -v --throttle 100 -p " + MonitorCenter.getAppPackageName() + " 1000"};
    }

}
