package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.MonitorCenter;

/**
 * Created by jexiong on 7/15/2015.
 */
public class DumpMemCommand extends Command<DumpMemCommand.MemoryBean> {

    private MemoryBean resultBean = new MemoryBean();

    @Override
    public MemoryBean getCommandOutput() {
        return resultBean;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb shell dumpsys meminfo " + MonitorCenter.getAppPackageName()};
    }

    public static class MemoryBean implements AbstractBean {

        public int nativeHeap;
        public int dalvikHeap;
        public int others;
        public int total;

        public void bind(String line) {
            line = line.trim();
            if (line.startsWith("Native Heap ")) {
                line = line.replaceAll("\\s+", " ");
                String[] values = line.split(" ");
                nativeHeap = Integer.valueOf(values[2]);
            } else if (line.startsWith("Dalvik Heap ")) {
                line = line.replaceAll("\\s+", " ");
                String[] values = line.split(" ");
                dalvikHeap = Integer.valueOf(values[2]);
            } else if (line.startsWith("TOTAL ") && !line.contains("TOTAL PSS:")) {
                line = line.replaceAll("\\s+", " ");
                String[] values = line.split(" ");
                total = Integer.valueOf(values[1]);
            }
        }
    }

}
