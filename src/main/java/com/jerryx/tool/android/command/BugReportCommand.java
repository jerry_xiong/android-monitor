package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BugReportCommand extends Command<AbstractBean> {

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{
                "adb bugreport"
        };
    }

    public void writeToFile() {
        if (getContent().length() == 0) {
            return;
        }
        String fileName = Utils.getLogPath() + Utils.getTimestampString() + "bugreport.txt";
        File file = new File(fileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(getContent().toString());
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
