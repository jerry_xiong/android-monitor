package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.MonitorException;
import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;

public abstract class Command<T extends AbstractBean> {

    public enum Status {
        PENDING, RUNNING, COMPLETE
    }

    protected Status status = Status.PENDING;
    protected boolean isLogging = true;
    protected StringBuilder content = new StringBuilder();
    protected Process process;
    protected MonitorException monitorException;

    public abstract String[] getCommandInputArray();

    public abstract T getCommandOutput();

    public void bindLine(String line) {
        content.append(line).append(Utils.NEW_LINE);
        getCommandOutput().bind(line);
    }

    public void bindProcess(Process process){
        this.process = process;
    }

    public void destroy() {
        if (process != null) {
            process.destroy();
        }
    }

    public boolean isLogging() {
        return isLogging;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public StringBuilder getContent() {
        return content;
    }

    public MonitorException getMonitorException() {
        return monitorException;
    }

    public void setMonitorException(MonitorException monitorException) {
        this.monitorException = monitorException;
    }

    public boolean containsError() {
        return this.monitorException != null;
    }

}
