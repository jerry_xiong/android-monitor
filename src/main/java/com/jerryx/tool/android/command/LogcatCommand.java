package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;

/**
 * Created by jexiong on 7/15/2015.
 */
public class LogcatCommand extends Command<AbstractBean> {

    @Override
    public AbstractBean getCommandOutput() {
        return AbstractBean.EMPTY_BEAN;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb -d logcat -t 1000 -v threadtime"};
    }

}
