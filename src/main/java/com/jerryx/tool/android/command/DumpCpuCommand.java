package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.MonitorCenter;

/**
 * Created by jexiong on 7/15/2015.
 */
public class DumpCpuCommand extends Command<DumpCpuCommand.CpuBean> {

    private CpuBean cpuBean = new CpuBean();

    @Override
    public CpuBean getCommandOutput() {
        return cpuBean;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb shell dumpsys cpuinfo"};
    }

    public static class CpuBean implements AbstractBean {

        public float user;
        public float kernel;
        public float total;

        public void bind(String line) {
            line = line.trim();
            String packageName = MonitorCenter.getAppPackageName() + ":";
            int index1 = line.indexOf(packageName);
            int indexRemote = line.indexOf(packageName + "remote");
            if (index1 != -1 && indexRemote == -1) {
                int indexTotal = line.indexOf("%");
                total = Float.valueOf(line.substring(0, indexTotal));

                int indexUser = line.indexOf("user");
                String userCPU = line.substring(index1 + packageName.length(), indexUser);

                user = Float.valueOf(userCPU.substring(0, userCPU.length() - 2));

                int indexKernel = line.indexOf("kernel");
                String kernelCpu = line.substring(indexUser + "user +".length(), indexKernel);

                kernel = Float.valueOf(kernelCpu.substring(0, kernelCpu.length() - 2));
            }
        }
    }
}
