package com.jerryx.tool.android.command;

/**
 * Created by Jerry on 7/15/15.
 */
public interface AbstractBean {

    void bind(String line);

    AbstractBean EMPTY_BEAN = new AbstractBean() {
        public void bind(String line) {

        }
    };
}
