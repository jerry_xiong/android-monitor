package com.jerryx.tool.android.command;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jexiong on 7/15/2015.
 */
public class DeviceInfoCommand extends Command<DeviceInfoCommand.DeviceInfoBean> {

    DeviceInfoBean resultBean = new DeviceInfoBean();

    @Override
    public DeviceInfoBean getCommandOutput() {
        return resultBean;
    }

    @Override
    public String[] getCommandInputArray() {
        return new String[]{"adb shell getprop"};
    }

    public static class DeviceInfoBean implements AbstractBean {

        private Map<String, String> infoMap = new HashMap<String, String>();

        public void bind(String line) {
            int indexSeparator = line.indexOf(":");
            if (indexSeparator != -1) {
                String key = line.substring(0, indexSeparator).trim();
                String value = line.substring(indexSeparator + 1).trim();
                key = key.substring(1, key.length() - 1);
                value = value.substring(1, value.length() - 1);
                infoMap.put(key, value);
            }
        }

        public Map<String, String> getInfoMap() {
            return infoMap;
        }
    }
}
