package com.jerryx.tool.android.command;

import com.jerryx.tool.android.util.MonitorException;
import com.jerryx.tool.android.util.ResultCode;
import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jerry on 7/15/15.
 */
public class CommandRunner {

    private static final boolean PRINT_DETAIL_LOG = false;
    private static Logger logger = Logger.getLogger(CommandRunner.class);

    private static CommandRunner INSTANCE = new CommandRunner();

    public static CommandRunner getInstance() {
        return INSTANCE;
    }

    public void runCommands(Command... commandArray) {
        for (Command command : commandArray) {
            runCommand(command);
        }
    }

    public void runCommand(Command command) {
        command.setStatus(Command.Status.RUNNING);
        StringBuilder full = new StringBuilder();
        StringBuilder error = new StringBuilder();
        BufferedReader stdin = null;
        BufferedReader errin = null;
        try {
            Process process = null;
            for (String commandElement : command.getCommandInputArray()) {
                process = Runtime.getRuntime().exec(commandElement);
                if (command.isLogging()) {
                    logger.debug("Starting to run command " + command + ", with element " + commandElement);
                }
            }
            command.bindProcess(process);

            stdin = new BufferedReader(new InputStreamReader(process.getInputStream()));
            errin = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String line = null;
            while ((line = stdin.readLine()) != null) {
                full.append(line);
                command.bindLine(line);
                full.append(Utils.NEW_LINE);
            }
            while ((line = errin.readLine()) != null) {
                error.append(line);
                error.append(Utils.NEW_LINE);
            }
            if (error.length() > 0) {
                logger.warn("\n=== ADB reports: ===");
                logger.warn(error);
                if (error.indexOf("error: device") !=  -1) {
                    command.setMonitorException(new MonitorException(ResultCode.ERROR_DEVICE_NOT_FOUND, "device not found"));
                } else if (error.indexOf("Exception") != -1 || error.indexOf("error") != -1) {
                    command.setMonitorException(new MonitorException(ResultCode.ERROR_DEVICE_ADB_GENERAL, "general error"));
                }
            }
        } catch (IOException e) {
            logger.error("IOException command error.", e);
            command.setMonitorException(new MonitorException(ResultCode.ERROR_DEVICE_IO, "io error"));
        } catch (Exception e) {
            logger.error("Exception command error.", e);
            command.setMonitorException(new MonitorException(ResultCode.ERROR_GENERAL, "general error"));
        } finally {
            try {
                stdin.close();
                errin.close();
            } catch (IOException e) {
                logger.error("closing io error.", e);
            }
            if (command.isLogging() && PRINT_DETAIL_LOG) {
                logger.debug("End to run command " + command);
                logger.debug(full.toString());
            }
            command.setStatus(Command.Status.COMPLETE);
        }
    }
}
