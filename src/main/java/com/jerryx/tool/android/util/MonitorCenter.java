package com.jerryx.tool.android.util;

import com.jerryx.tool.android.command.ReportCommand;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by jexiong on 7/17/2015.
 */
public class MonitorCenter {

    public static final AtomicBoolean IS_ADB_CONNECTED = new AtomicBoolean(false);

    public static final AtomicBoolean IS_MONITORING = new AtomicBoolean(false);

    public static final AtomicBoolean IS_RECORDING = new AtomicBoolean(false);

    public static final AtomicBoolean IS_MONKEY_RUNNING = new AtomicBoolean(false);

    public static final ReportCommand.ReportBean REPORT_BEAN = new ReportCommand.ReportBean();

    private static String DEFAULT_PACKAGE_NAME;

    public static synchronized void setAppPackageName(String packageName) {
        DEFAULT_PACKAGE_NAME = packageName;
    }

    public static synchronized String getAppPackageName() {
        return DEFAULT_PACKAGE_NAME;
    }

}
