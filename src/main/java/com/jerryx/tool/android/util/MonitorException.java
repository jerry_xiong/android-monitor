package com.jerryx.tool.android.util;

/**
 * Created by Jerry on 7/16/15.
 */
public class MonitorException extends Exception {

    private int errorCode;

    public MonitorException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
