package com.jerryx.tool.android.util;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Created by jexiong on 7/15/2015.
 */
public class Utils {

    public static final String NEW_LINE = "\r\n";
    public static final int DELAY = 2000;

    private static Properties DEFAULT_CONFIG = new Properties();
    private static Properties TEST_CONFIG = new Properties();

    private static ResourceBundle MESSAGE_CONFIG = null;

    static {
        try {
            DEFAULT_CONFIG.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
            TEST_CONFIG.load(new FileInputStream(new File("data/testloader.properties")));
            MESSAGE_CONFIG = ResourceBundle.getBundle("msg", Locale.US);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Dimension getDefaultDimensionGap() {
        return new Dimension(20, 20);
    }

    public static String getTimestampString() {
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return format.format(new Date());
    }

    public static String getConfigValue(String key) {
        return DEFAULT_CONFIG.getProperty(key);
    }

    public static String getTestConfigValue(String key) {
        return TEST_CONFIG.getProperty(key);
    }

    public static String getArtifactPath() {
        return getConfigValue("app.artifact.storage.path");
    }

    public static String getLogPath() {
        return getConfigValue("app.report.folder");
    }

    public static String getAutomationPath() {
        return getConfigValue("app.automation.storage.path");
    }

    public static String getMessage(String key) {
        return MESSAGE_CONFIG.getString(key);
    }

}
