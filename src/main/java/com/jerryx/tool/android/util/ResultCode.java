package com.jerryx.tool.android.util;

/**
 * Created by Jerry on 7/16/15.
 */
public interface ResultCode {

    public static final int ERROR_DEVICE_NOT_FOUND = 101;
    public static final int ERROR_DEVICE_TOO_MORE = 102;
    public static final int ERROR_DEVICE_ADB_GENERAL = 103;
    public static final int ERROR_DEVICE_IO = 104;
    public static final int ERROR_GENERAL = 500;
}
