
package com.jerryx.tool.android.chart;

import com.jerryx.tool.android.MainController;
import com.jerryx.tool.android.command.*;
import com.jerryx.tool.android.util.MonitorCenter;
import com.jerryx.tool.android.util.ResultCode;
import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;
import org.jfree.data.time.Millisecond;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static com.jerryx.tool.android.util.Utils.getMessage;
import static com.jerryx.tool.android.util.Utils.getTestConfigValue;

/**
 * Created by jexiong on 10/23/2015.
 */
public class CommandPanel extends JPanel {

    final JButton startButton = new JButton(getMessage("msg.start.monitor"));
    final JButton screenshotButton = new JButton(getMessage("msg.screenshot.title"));
    final JButton recordButton = new JButton(getMessage("msg.start.record"));
    final JButton exportButton = new JButton(getMessage("msg.report.export"));
    final JButton monkeyButton = new JButton(getMessage("msg.start.monkey"));
    final JButton cleanUpButton = new JButton(getMessage("msg.clean.resource"));
    final JButton runAutomation = new JButton(getMessage("msg.run.automation"));
    final JButton analyzeBattery = new JButton(getMessage("msg.battery.report"));

    final JProgressBar progressBar = new JProgressBar();
    final JProgressBar progressBar2 = new JProgressBar();

    public CommandPanel() {
        init();
    }

    private void init() {

        setLayout(new BoxLayout(CommandPanel.this, BoxLayout.Y_AXIS));

        setBorder(new TitledBorder(getMessage("msg.command.center")));

        initStartAction();

        initScreenshotAction();

        initRecordAction();

        initExportAction();

        initCleanAction();

        initMonkeyAction();

        initAutomationAction();

        initBatteryAction();
    }

    private void initRecordAction() {
        final RecordCommand recordCommand = new RecordCommand();
        recordButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (MonitorCenter.IS_RECORDING.compareAndSet(false, true)) {
                    recordButton.setText(getMessage("msg.stop.record"));
                    recordButton.setBackground(Color.RED);
                    new Thread(new Runnable() {
                        public void run() {
                            CommandRunner.getInstance().runCommand(recordCommand);
                            MonitorCenter.REPORT_BEAN.getFileList().add(recordCommand.getFileName());
                            if (recordCommand.containsError()) {
                                JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.record.fail"),
                                        getMessage("msg.record.title"), JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    }).start();
                } else if (MonitorCenter.IS_RECORDING.compareAndSet(true, false)) {
                    recordButton.setText(getMessage("msg.start.record"));
                    recordButton.setBackground(null);
                    recordCommand.destroy();
                }
            }
        });
        add(recordButton);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    private void initStartAction() {
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                if (MonitorCenter.IS_MONITORING.compareAndSet(true, false)) {
                    startButton.setText(getMessage("msg.start.monitor"));
                    startButton.setBackground(null);
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.monitor.stopping"),
                            getMessage("msg.monitor.title"), JOptionPane.INFORMATION_MESSAGE);
                } else if (MonitorCenter.IS_MONITORING.compareAndSet(false, true)) {
                    new Thread(new Runnable() {
                        public void run() {
                            while (MonitorCenter.IS_MONITORING.get()) {
                                try {
                                    Thread.sleep(Utils.DELAY);
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                }
                                Millisecond millisecond = new Millisecond();
                                DumpCpuCommand dumpCpuCommand = new DumpCpuCommand();
                                DumpMemCommand dumpMemCommand = new DumpMemCommand();
                                CommandRunner.getInstance().runCommands(dumpCpuCommand, dumpMemCommand);
                                if (dumpCpuCommand.containsError() &&
                                        dumpCpuCommand.getMonitorException().getErrorCode() == ResultCode.ERROR_DEVICE_NOT_FOUND) {
                                    MonitorCenter.IS_ADB_CONNECTED.compareAndSet(true, false);
                                } else {
                                    MonitorCenter.IS_ADB_CONNECTED.compareAndSet(false, true);
                                }
                                MainController.getInstance().getDeviceInfoPanel().onAdbStatusChanged();
                                MainController.getInstance().getCpuChart().update(millisecond, dumpCpuCommand.getCommandOutput());
                                MainController.getInstance().getMemChart().update(millisecond, dumpMemCommand.getCommandOutput());
                            }
                        }
                    }).start();
                    startButton.setText(getMessage("msg.stop.monitor"));
                    startButton.setBackground(Color.RED);
                    startButton.setOpaque(true);
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.monitor.starting"),
                            getMessage("msg.monitor.title"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        add(startButton);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    private void initMonkeyAction() {
        final MonkeyCommand monkeyCommand = new MonkeyCommand();
        monkeyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (MonitorCenter.IS_MONKEY_RUNNING.compareAndSet(false, true)) {
                    monkeyButton.setText(getMessage("msg.stop.monkey"));
                    monkeyButton.setBackground(Color.RED);
                    monkeyButton.setOpaque(true);
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.run.monkey.start"),
                            getMessage("msg.monkey.title"), JOptionPane.INFORMATION_MESSAGE);
                    new Thread(new Runnable() {
                        public void run() {
                            CommandRunner.getInstance().runCommand(monkeyCommand);
                            if (monkeyCommand.containsError()) {
                                JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.run.monkey.fail"),
                                        getMessage("msg.monkey.title"), JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    }).start();
                } else if (MonitorCenter.IS_MONKEY_RUNNING.compareAndSet(true, false)) {
                    monkeyButton.setText(getMessage("msg.start.monkey"));
                    monkeyButton.setBackground(null);
                    monkeyCommand.destroy();
                }
            }
        });
        add(monkeyButton);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    private void initExportAction() {
        exportButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ReportCommand reportCommand = new ReportCommand();
                CommandRunner.getInstance().runCommand(reportCommand);
                if (reportCommand.containsError()) {
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.report.gen.fail"),
                            getMessage("msg.report.title"), JOptionPane.WARNING_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.report.gen.success"),
                            getMessage("msg.report.title"), JOptionPane.INFORMATION_MESSAGE);
                    JOptionPane.showMessageDialog(CommandPanel.this, MonitorCenter.REPORT_BEAN.toString(),
                            getMessage("msg.report.title"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        add(exportButton);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    private void initScreenshotAction() {
        screenshotButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CaptureCommand captureCommand = new CaptureCommand();
                CommandRunner.getInstance().runCommand(captureCommand);
                MainController.getInstance().getCpuChart().exportSnapshot(Utils.getTimestampString());
                MainController.getInstance().getMemChart().exportSnapshot(Utils.getTimestampString());
                MonitorCenter.REPORT_BEAN.getFileList().add(captureCommand.getFileName());
                if (captureCommand.containsError()) {
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.screen.capture.fail"),
                            getMessage("msg.screen.capture.title"), JOptionPane.WARNING_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.screen.capture.success"),
                            getMessage("msg.screen.capture.title"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        add(screenshotButton);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    private void initCleanAction() {
        cleanUpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CleanCommand cleanCommand = new CleanCommand();
                CommandRunner.getInstance().runCommand(cleanCommand);
                if (cleanCommand.containsError()) {
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.clean.resource.fail"),
                            getMessage("msg.clean.resource"), JOptionPane.WARNING_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.clean.resource.success"),
                            getMessage("msg.clean.resource"), JOptionPane.INFORMATION_MESSAGE);
                }
                DeviceInitCommand deviceInitCommand = new DeviceInitCommand();
                CommandRunner.getInstance().runCommand(deviceInitCommand);
            }
        });
        add(cleanUpButton);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    private void initAutomationAction() {
        final ArrayList<String> testCases = new ArrayList<String>();

        int testCaseCount = Integer.valueOf(getTestConfigValue("total.test.case"));

        for (int i = 0; i < testCaseCount; i++) {
            testCases.add(getTestConfigValue("test.class." + i));
        }

        final String[] testArray = testCases.toArray(new String[testCases.size()]);
        runAutomation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final String selectedCase = (String) JOptionPane.showInputDialog(CommandPanel.this,
                        getMessage("msg.select.automation.case"),
                        getMessage("msg.available.automation.cases"),
                        JOptionPane.QUESTION_MESSAGE, null,
                        testArray,
                        testArray[0]);
                if (selectedCase == null) {
                    return;
                }
                runAutomation.setBackground(Color.RED);
                runAutomation.setEnabled(false);
                progressBar.setIndeterminate(true);
                progressBar.setVisible(true);
                CommandPanel.this.add(progressBar);
                CommandPanel.this.repaint();
                CommandPanel.this.revalidate();
                AutomationWorker automationWorker = new AutomationWorker(selectedCase);
                automationWorker.execute();
            }
        });
        add(runAutomation);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    class AutomationWorker extends SwingWorker<RunTestCommand.TestBean, Void> {

        private Logger logger = Logger.getLogger(AutomationWorker.class);

        private String testCaseName;

        public AutomationWorker(String testCaseName) {
            this.testCaseName = testCaseName;
        }

        @Override
        protected RunTestCommand.TestBean doInBackground() {
            RunTestCommand runTestCommand = new RunTestCommand(testCaseName);
            progressBar.setString("Running " + runTestCommand.getShortTestCaseName());
            progressBar.setStringPainted(true);
            CommandRunner.getInstance().runCommand(runTestCommand);
            logger.debug("Automation test result: " + runTestCommand.getCommandOutput().getContent());
            runTestCommand.getCommandOutput().verify();
            if (runTestCommand.containsError()) {
                JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.run.automation.fail"),
                        getMessage("msg.run.automation"), JOptionPane.WARNING_MESSAGE);
            }
            return runTestCommand.getCommandOutput();
        }

        @Override
        protected void done() {
            RunTestCommand.TestBean testBean = null;
            try {
                testBean = get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if (testBean.isPassed()) {
                String successMsg = getMessage("msg.run.automation.success");
                successMsg = String.format(successMsg, testCaseName, testBean.totalCount, testBean.passCount, testBean.failedCount);
                JOptionPane.showMessageDialog(CommandPanel.this, successMsg,
                        getMessage("msg.result.automation"), JOptionPane.INFORMATION_MESSAGE);
            } else {
                String failMsg = getMessage("msg.run.automation.fail");
                failMsg = String.format(failMsg, testCaseName, testBean.totalCount, testBean.passCount, testBean.failedCount);
                JOptionPane.showMessageDialog(CommandPanel.this, failMsg,
                        getMessage("msg.result.automation"), JOptionPane.WARNING_MESSAGE);
            }
            progressBar.setIndeterminate(false);
            progressBar.setVisible(false);
            runAutomation.setBackground(null);
            runAutomation.setEnabled(true);
        }
    }

    private void initBatteryAction() {
        analyzeBattery.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                progressBar2.setIndeterminate(true);
                progressBar2.setVisible(true);
                progressBar2.setString(getMessage("msg.battery.dump.bugreport"));
                progressBar2.setStringPainted(true);
                analyzeBattery.setBackground(Color.RED);
                analyzeBattery.setEnabled(false);
                CommandPanel.this.add(progressBar2);
                CommandPanel.this.repaint();
                CommandPanel.this.revalidate();
                new BugReportWorker().execute();
            }
        });
        add(analyzeBattery);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    class BugReportWorker extends SwingWorker<Void, Void> {

        @Override
        protected Void doInBackground() {
            BugReportCommand bugReportCommand = new BugReportCommand();
            OpenUrlCommand openUrlCommand = new OpenUrlCommand(getTestConfigValue("battery.historian.host"));
            CommandRunner.getInstance().runCommand(bugReportCommand);
            if (bugReportCommand.containsError()) {
                JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.battery.report.fail"),
                        getMessage("msg.battery.report"), JOptionPane.WARNING_MESSAGE);
            } else {
                bugReportCommand.writeToFile();
                JOptionPane.showMessageDialog(CommandPanel.this, getMessage("msg.battery.report.success"),
                        getMessage("msg.battery.report"), JOptionPane.INFORMATION_MESSAGE);
                CommandRunner.getInstance().runCommand(openUrlCommand);
            }
            return null;
        }

        @Override
        protected void done() {
            progressBar2.setIndeterminate(false);
            progressBar2.setVisible(false);
            analyzeBattery.setBackground(null);
            analyzeBattery.setEnabled(true);
        }
    }
}
