package com.jerryx.tool.android.chart;

import com.jerryx.tool.android.command.CommandRunner;
import com.jerryx.tool.android.command.DumpMemCommand;
import com.jerryx.tool.android.util.MonitorCenter;
import com.jerryx.tool.android.util.MonitorException;
import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static com.jerryx.tool.android.util.Utils.getMessage;

/**
 * Created by Jerry on 7/15/15.
 */
public class RealTimeMemChart extends ChartPanel {

    private static RealTimeMemChart INSTANCE = new RealTimeMemChart();

    private static Logger logger = Logger.getLogger(RealTimeMemChart.class);

    private static final long serialVersionUID = 1L;
    private TimeSeries timeSeries;
    private TimeSeries timeSeries2;
    private TimeSeries timeSeries3;

    private RealTimeMemChart() {
        super(null);
        super.setChart(createChart());
    }

    public static RealTimeMemChart getInstance() {
        return INSTANCE;
    }

    private JFreeChart createChart() {
        timeSeries = new TimeSeries(getMessage("chart.label.dheap"), Millisecond.class);
        timeSeries2 = new TimeSeries(getMessage("chart.label.nheap"), Millisecond.class);
        timeSeries3 = new TimeSeries(getMessage("chart.label.total"), Millisecond.class);
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection(
                timeSeries);
        timeSeriesCollection.addSeries(timeSeries2);
        timeSeriesCollection.addSeries(timeSeries3);
        String title = String.format(getMessage("chart.title.mem"), MonitorCenter.getAppPackageName());
        JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(
                title,
                getMessage("chart.title.time"),
                getMessage("chart.title.mem.y"),
                timeSeriesCollection, true, true, false);
        XYPlot xyplot = jfreechart.getXYPlot();
        ValueAxis valueaxis = xyplot.getDomainAxis();
        valueaxis.setAutoRange(true);
        valueaxis.setFixedAutoRange(30000D);
        valueaxis = xyplot.getRangeAxis();
        // set y axis limit
        valueaxis.setRange(0.0D, 350000D);
        return jfreechart;
    }

    public void update(Millisecond millisecond, DumpMemCommand.MemoryBean memoryBean) {
        timeSeries.add(millisecond, memoryBean.dalvikHeap);
        timeSeries2.add(millisecond, memoryBean.nativeHeap);
        timeSeries3.add(millisecond, memoryBean.total);
        logger.debug(String.format("Memory Usage KB -- dalvikHeap: %s, nativeHeap: %s, total: %s",
                memoryBean.dalvikHeap, memoryBean.nativeHeap, memoryBean.total));
        if (memoryBean.total > MonitorCenter.REPORT_BEAN.getMaxMemoryUsage()) {
            MonitorCenter.REPORT_BEAN.setMaxMemoryTime(new Date());
            MonitorCenter.REPORT_BEAN.setMaxMemoryUsage(memoryBean.total);
        }
    }

    public void exportSnapshot(String timeStamp) {
        File file = new File("logs/" + timeStamp + "mem_snapshot.png");
        try {
            ChartUtilities.saveChartAsJPEG(file, super.getChart(), 800, 600);
            logger.debug("Export memory snapshot successfully.");
        } catch (IOException e) {
            logger.error("Export memory snapshot successfully.");
        }
    }
}
