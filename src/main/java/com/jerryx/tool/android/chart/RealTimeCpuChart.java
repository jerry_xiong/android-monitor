package com.jerryx.tool.android.chart;

import com.jerryx.tool.android.command.CommandRunner;
import com.jerryx.tool.android.command.DumpCpuCommand;
import com.jerryx.tool.android.util.MonitorCenter;
import com.jerryx.tool.android.util.MonitorException;
import com.jerryx.tool.android.util.Utils;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static com.jerryx.tool.android.util.Utils.getMessage;

/**
 * Created by Jerry on 7/15/15.
 */
public class RealTimeCpuChart extends ChartPanel {

    private static Logger logger = Logger.getLogger(RealTimeCpuChart.class);

    private static final long serialVersionUID = 1L;
    private TimeSeries timeSeries;
    private TimeSeries timeSeries2;
    private TimeSeries timeSeries3;

    private static RealTimeCpuChart INSTANCE = new RealTimeCpuChart();

    public static RealTimeCpuChart getInstance() {
        return INSTANCE;
    }

    private RealTimeCpuChart() {
        super(null);
        super.setChart(createChart());
    }

    private JFreeChart createChart() {
        timeSeries = new TimeSeries(getMessage("chart.label.user"), Millisecond.class);
        timeSeries2 = new TimeSeries(getMessage("chart.label.kernel"), Millisecond.class);
        timeSeries3 = new TimeSeries(getMessage("chart.label.total"), Millisecond.class);
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(
                timeSeries);
        timeseriescollection.addSeries(timeSeries2);
        timeseriescollection.addSeries(timeSeries3);
        String title = String.format(getMessage("chart.title.cpu"), MonitorCenter.getAppPackageName());
        JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(
                title,
                getMessage("chart.title.time"),
                getMessage("chart.title.cpu.y"),
                timeseriescollection, true, true, false);
        XYPlot xyplot = jfreechart.getXYPlot();
        ValueAxis valueaxis = xyplot.getDomainAxis();
        valueaxis.setAutoRange(true);
        valueaxis.setFixedAutoRange(30000D);
        valueaxis = xyplot.getRangeAxis();
        // set y axis limit
        valueaxis.setRange(0.0D, 100D);
        return jfreechart;
    }

    public void update(Millisecond millisecond, DumpCpuCommand.CpuBean cpuBean) {
        timeSeries.add(millisecond, cpuBean.user);
        timeSeries2.add(millisecond, cpuBean.kernel);
        timeSeries3.add(millisecond, cpuBean.total);
        logger.debug(String.format("CPU Usage Percentage  -- user: %s, kernel: %s, total: %s", cpuBean.user, cpuBean.kernel, cpuBean.total));
        if (cpuBean.total > MonitorCenter.REPORT_BEAN.getMaxCpuUsage()) {
            MonitorCenter.REPORT_BEAN.setMaxCpuTime(new Date());
            MonitorCenter.REPORT_BEAN.setMaxCpuUsage(cpuBean.total);
        }
    }

    public void exportSnapshot(String timeStamp) {
        File file = new File("logs/" + timeStamp + "cpu_snapshot.png");
        try {
            ChartUtilities.saveChartAsJPEG(file, super.getChart(), 800, 600);
            logger.debug("Export cpu snapshot successfully.");
        } catch (IOException e) {
            logger.error("Export cpu snapshot fail.");
        }
    }
}
