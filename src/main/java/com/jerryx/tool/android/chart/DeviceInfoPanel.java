package com.jerryx.tool.android.chart;

import com.jerryx.tool.android.command.CommandRunner;
import com.jerryx.tool.android.command.DeviceInfoCommand;
import com.jerryx.tool.android.command.DeviceInitCommand;
import com.jerryx.tool.android.util.MonitorCenter;
import com.jerryx.tool.android.util.Utils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.jerryx.tool.android.util.Utils.getMessage;

/**
 * Created by jexiong on 7/15/2015.
 */
public class DeviceInfoPanel extends JPanel {

    private static DeviceInfoPanel INSTANCE = new DeviceInfoPanel();

    public static DeviceInfoPanel getInstance() {
        return INSTANCE;
    }

    private static Map<String, String> KEY_MAP = new ConcurrentHashMap<String, String>();

    public JLabel adbStatus = new JLabel();

    static {
        KEY_MAP.put("ro.product.model", "Model");
        KEY_MAP.put("ro.product.manufacturer", "Manufacturer");
        KEY_MAP.put("ro.build.version.release", "Android Version");
        KEY_MAP.put("ro.build.version.sdk", "SDK Level");
        KEY_MAP.put("ro.product.cpu.abi", "CPU Arch");
        KEY_MAP.put("gsm.version.ril-impl", "GSM Version");
        KEY_MAP.put("ro.bootimage.build.date", "Bootimage Build Date");
        KEY_MAP.put("ro.boot.serialno", "Serialno");
        KEY_MAP.put("wlan.driver.status", "WiFi Status");
        KEY_MAP.put("gsm.sim.state", "SIM State");
        KEY_MAP.put("gsm.sim.operator.alpha", "Operator");
        KEY_MAP.put("dalvik.vm.heapgrowthlimit", "Heap Limit");
        KEY_MAP.put("persist.sys.timezone", "Time Zone");
        KEY_MAP.put("ro.product.locale", "Locale");
    }

    private DeviceInfoPanel() {
        super.setBorder(new TitledBorder("Device Info"));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
        add(adbStatus);
        add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
    }

    public void init() {
        DeviceInitCommand deviceInitCommand = new DeviceInitCommand();
        DeviceInfoCommand deviceInfoCommand = new DeviceInfoCommand();
        CommandRunner.getInstance().runCommands(deviceInitCommand, deviceInfoCommand);
        if (deviceInitCommand.containsError()) {
            MonitorCenter.IS_ADB_CONNECTED.compareAndSet(true, false);
        } else {
            MonitorCenter.IS_ADB_CONNECTED.compareAndSet(false, true);
        }
        onAdbStatusChanged();
        for (String key : KEY_MAP.keySet()) {
            String value = deviceInfoCommand.getCommandOutput().getInfoMap().get(key);
            String labelTemplate = getMessage("msg.device.info.label");
            String finalLabel = String.format(labelTemplate, KEY_MAP.get(key), value);
            JLabel label = new JLabel(finalLabel);
            add(label);
            add(Box.createRigidArea(Utils.getDefaultDimensionGap()));
        }
        revalidate();
    }

    public synchronized void onAdbStatusChanged() {
        if (MonitorCenter.IS_ADB_CONNECTED.get()) {
            adbStatus.setText(getMessage("msg.connected.html"));
        } else {
            adbStatus.setText(getMessage("msg.disconnected.html"));
        }
    }

}
