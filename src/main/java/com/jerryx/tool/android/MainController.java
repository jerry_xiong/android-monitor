package com.jerryx.tool.android;

import com.jerryx.tool.android.chart.*;
import com.jerryx.tool.android.command.CheckPackageCommand;
import com.jerryx.tool.android.command.CommandRunner;
import com.jerryx.tool.android.util.MonitorCenter;
import com.jerryx.tool.android.util.MonitorException;
import com.jerryx.tool.android.util.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static com.jerryx.tool.android.util.Utils.getMessage;

/**
 * Created by Jerry on 7/15/15.
 */
public class MainController {

    public static void main(String[] args) {
        INSTANCE.init();
    }

    private static MainController INSTANCE = new MainController();

    public static MainController getInstance() {
        return INSTANCE;
    }

    private JFrame frame;

    private MainController() {
        frame = new JFrame(Utils.getConfigValue("app.name") + " " + Utils.getConfigValue("app.version"));
    }

    private RealTimeCpuChart cpuChart;
    private RealTimeMemChart memChart;
    private DeviceInfoPanel deviceInfoPanel;

    public DeviceInfoPanel getDeviceInfoPanel() {
        return deviceInfoPanel;
    }

    public RealTimeCpuChart getCpuChart() {
        return cpuChart;
    }

    public RealTimeMemChart getMemChart() {
        return memChart;
    }

    public void init() {

        while (MonitorCenter.getAppPackageName() == null) {

            String userInputPackage = JOptionPane.showInputDialog(frame, getMessage("msg.input.package"),
                    getMessage("msg.init"), JOptionPane.INFORMATION_MESSAGE).trim();

            CheckPackageCommand checkPackageCommand = new CheckPackageCommand();

            CommandRunner.getInstance().runCommand(checkPackageCommand);

            if (checkPackageCommand.containsError()) {
                int optionValue = JOptionPane.showConfirmDialog(frame, getMessage("msg.enable.usb"),
                        getMessage("msg.init"), JOptionPane.YES_NO_OPTION);
                if (optionValue == JOptionPane.NO_OPTION) {
                    System.exit(0);
                }
            } else {
                if (checkPackageCommand.getCommandOutput().containsPackage(userInputPackage)) {
                    MonitorCenter.setAppPackageName(userInputPackage);
                } else {
                    JOptionPane.showMessageDialog(frame, getMessage("msg.package.invalid"));
                }
            }
        }

        cpuChart = RealTimeCpuChart.getInstance();
        memChart = RealTimeMemChart.getInstance();
        deviceInfoPanel = DeviceInfoPanel.getInstance();

        renderLayout();
    }

    private void renderLayout() {
        JPanel monitorPanel = new JPanel();
        monitorPanel.setLayout(new BoxLayout(monitorPanel, BoxLayout.Y_AXIS));
        monitorPanel.add(cpuChart);
        monitorPanel.add(memChart);
        frame.getContentPane().add(monitorPanel, new BorderLayout().CENTER);
//        frame.getContentPane().add(logConsole, new BorderLayout().SOUTH);
        frame.getContentPane().add(deviceInfoPanel, new BorderLayout().WEST);
        CommandPanel commandPanel = new CommandPanel();
        frame.getContentPane().add(commandPanel, new BorderLayout().EAST);
        frame.setSize(1280, 1000);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
                                    @Override
                                    public void windowClosing(WindowEvent e) {
                                        System.exit(0);
                                    }
                                }
        );
        deviceInfoPanel.init();
    }
}
