This tool is designed for Android performance testing for any android apps.

### Setup
1) You need to make sure the JDK 1.7+ has been installed on your Windows or Mac.

2) You need to make sure the Android SDK (4.x or 5.x) has been installed on your Windows or Mac.

3) You need to make sure your device's USB Debugging is enabled before starting.

4) This tool is just a runnable jar file. If you have the android.monitor-1.x-SNAPSHOT-jar-with-dependencies.jar, you can click to start it.
Or in cmd, execute "java -jar android.monitor-1.x-SNAPSHOT-jar-with-dependencies.jar", then you can see runtime log in cmd.

5) You need to input your android app's package name. You can find it from your device setting on running process page.

### Functions
1) You can use it to monitor CPU and Memory usage by your app.

2) You can take screenshots during monitor process.

3) You can record device screen during monitor process.

4) You can execute a simple monkey program (1000 user events) during monitor process.

5) Run android tests. The tool will load test classes from data/testsloader.txt file. In addition, all you test data should be placed in data/automation.properties file.

6) You can export all of device screenshots/videos into logs folder by EXPORT. 
By default, you can find all of the original files from device /sdcard/jerrytool/artifact/.
The jerrytool folder will be created when you first time to run this tool.

7) You can use this tool to analyze your device battery usage by your app. I have already integrated google's battery-historian 2.0 with this tool. 
Please note that you must un-charge USB if you want to watch battery usage for your operations. 
In a word, what you need is to use your app like a normal user when un-charging. 
After you done your operations, you need to re-charge USB and click "Analyze Battery Drain" and wait for a bugreport to be complete dumped. 
Last, the tool will bring you to my internal battery-historian server and you can just upload the bugreport under logs folder to see the final battery analysis.
Refer to https://github.com/google/battery-historian to understand battery-historian 2.0.

8) The runtime log tracking for memory/cpu/automation are locating into logs folder (cpu_log.txt, mem_log.txt, test_log.txt, rolling new one by 1MB for each).

### Known Issues
* The monkey program can't be force-stopped during monitor. You have to wait for the 1000 events to be executed complete.
* Sometimes, the total CPU usage will be more than 100% if your device is multi-core, in this case you will not see the corresponding curve from chart for a while.
* After you click "Export Report", all of files under /sdcard/jerrytool/artifact/ will be exported into your PC/MAC logs folder. So, you may need to delete some files that already out of date by click "Clean Gen Resource" button.

### Contact
Please send an email to jerryx.good@gmail.com if you have any question on this tool.